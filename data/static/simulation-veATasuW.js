<div class="text-center lazy-load"><canvas id="a081e536ec67"></canvas></div>

<div class="text-center">
<div class="btn-group" role="group">
    <input class="btn btn-primary w-100" id="startStop_a081e536ec67" type="button" onclick="instance_a081e536ec67.startStop()" value="Start">
    <input class="btn btn-primary w-100" id="step_a081e536ec67" type="button" onclick="instance_a081e536ec67.nextFrame()" value="Step">
    <input class="btn btn-primary w-100" id="reset_a081e536ec67" type="button" onclick="instance_a081e536ec67.reset()" value="Reset">
</div>
</div>

<script>
class a081e536ec67{

    constructor(){
        this.canvas = document.getElementById('a081e536ec67');
        this.startStopButton = document.getElementById('startStop_a081e536ec67');
        this.stepButton = document.getElementById('step_a081e536ec67');
        this.ctx = this.canvas.getContext('2d');

        this.resolution = 3;

        this.rows = 64;
        this.cols = 64;

        this.running = false;

        this.canvas.height  = this.resolution * this.rows;
        this.canvas.width   = this.resolution * this.cols;
    }

    initGrid(){
        this.grid = new Array(this.rows).fill(null);
        for (let i=0; i<this.grid.length; i++){
            this.grid[i] = new Array(this.cols).fill(null).map(() => Math.floor(2.0*Math.random()));
        }
        return this.grid;
    }
  
    render(grid){
        for (let r = 0; r<this.rows; r++){
            for (let c = 0; c < this.cols; c++){
                const cell = grid[r][c];
                this.ctx.beginPath();        
                this.ctx.strokeStyle = '#8e8e8e';
                this.ctx.fillStyle = (cell ? '#fcf379' : '#303030');
                this.ctx.rect(c*this.resolution,r*this.resolution,this.resolution,this.resolution);
                this.ctx.fill();
                //ctx.stroke();
            }
        }
    }

    reset(){
        this.g = this.initGrid();
        this.render(this.g);
    }

    nextFrame(){
        this.g = this.update(this.g);
        this.render(this.g);

        if (this.running){
            //window.setTimeout(this.nextFrame,30);
            window.setTimeout(this.nextFrame.bind(this),30);
        }
    }

    startStop(){
        this.running = !this.running;
        if (this.running){
            this.startStopButton.value = "Stop";
            this.stepButton.disabled = true;
            this.nextFrame();
        }
        else{
            this.startStopButton.value = "Start";
            this.stepButton.disabled = false;
        }
    }

    update(grid){
        const tmp = grid.map(arr => [...arr]);

        for (let r = 0; r<this.rows; r++){
            for (let c = 0; c < this.cols; c++){

                const cell = tmp[r][c];

                var neighbor_sum = 0;
                for (let i = -1;i<2;i++){
                    for (let j=-1;j<2;j++){
                        if (i===0 && j===0){
                            continue;
                        }
                        if (r+i<0 || c+j<0){
                            continue;
                        }
                        if (r+i>=this.rows || j+i>=this.cols){
                            continue;
                        }

                        const curr_neighb = tmp[r+i][c+j];
                        neighbor_sum += curr_neighb;
                    }
                }
                if (!cell && neighbor_sum===3) {
                    grid[r][c] = 1;
                }
                else if (cell && neighbor_sum>3){
                    grid[r][c] = 0;
                }
                else if (cell && (neighbor_sum==2 || neighbor_sum==3)){
                    grid[r][c] = 1;
                }
                else{
                    grid[r][c] = 0;
                }
            }
        }
        return grid;
    }
}

let instance_a081e536ec67 = new a081e536ec67();
instance_a081e536ec67.reset();
</script>