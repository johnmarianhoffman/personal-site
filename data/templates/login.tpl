{{define "content"}}
<div class="container mx-auto mt-auto mb-auto" style="width:300px">
  <h1>Login</h1>      
  <form method="post" action="/login">
    <div class="form-group">
      <label for="username">Username</label>
      <input type="text" id="username" name="username"></input>
    </div>

    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" id="password" name="password"></input>
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
{{end}}