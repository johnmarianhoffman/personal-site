<!-- This template is to provide some sort of standardized base for project pages -->

{{define "content"}}

{{with .Project}}
<div class="container">
	
    <hr>
	<h2 style="text-align:center;">{{.Title}} </h2>
	
	{{ range .Contributors }}
	<p style="text-align:center;margin-bottom:5px;">
		{{.First}} {{.Last}}{{if .Affiliation}}<sup><a href="{{.Affiliation}}">*</a></sup>{{end}}
	</p>
	{{end}}
	
	<br>
	
	<figure>
		<img src="{{.MainPhoto}}" class="mx-auto d-block img-fluid" style="border-radius:10px;height:auto;max-width:80%;" alt=""></img>
		<figcaption style="text-align:center;"><em>{{.MainCaption}}</em></figcaption>
	</figure>
	
	<h4 style="text-align:center;">Abstract</h4>

	<div class="row">
		<div class="col">
			
		</div>
		<div class="col-10">
			<p>{{.Abstract}}</p>
		</div>
		<div class="col">
			
		</div>
	</div>

	<hr>
	<br>
	
	{{.Body}} <!-- body here should be HTML and handled case-by-case per project -->
	
	<h3>Links</h3>
	<div class="row">
		<div class="col-sm">		
			<ul>
				{{ range .RelevantLinks }}
				<li><a href="{{.}}">{{.}}</a></li>
				{{ end }}
			</ul>
		</div>
	</div>
	
	<h3>Citations</h3>
	<div class="row">
		<div class="col-sm">
			<ol>
				{{ range .Citations }}
				<li>{{.}}</li>
				{{ end }}
			</ol>
		</div>
	</div>

	{{ with .AdditionalPhotos }}
	<h3>Additional Photos</h3>
	{{ range .AdditionalPhotos }}
	<div class="col-sm">
		<a href="{{.}}"><img src="{{.}}" class="mx-auto d-block img-fluid" style="border-radius:25px" alt=""></img></a>
	</div>
	{{ end }}
	{{ end}}
	
</div>
{{end}}
{{end}}
