{{define "content"}}

<h1 style="text-align:center;">About this site</h1>

<p>Building and running this site is a hobby project of mine that I do in my spare time.  In addition developing all of the site software, I also run all of the networking and server equipment out of my apartment.  High risk, low reward? A little, but I enjoy wrangling my own little corner of the internet in the apartment with me.</p>

<img src="/static/rack-photo.png" class="mx-auto d-block img-fluid" max-width="50" style="max-width: 40%;border-radius:25px;" alt="photograph of john's server rack with the webserver labeled with a red x and a 'you are here!' sign">

<p>Some info:</p>
<ul>
  <li>Site/server is 100% <a href="https://go.dev">Go</a> with templates</li>
  <li>Previous version was <a href="https://www.djangoproject.com/">Django</a>, but I found it slow and cumbersome, and WSGI an annoying complication</li>
  <li>Styling is essentially minimum bootstrap to get decent behavior on different devices</li>
  <li>Styling is not my first concern (i.e. I'm bad at it), so if you have any thoughts to improve your experience, please share!</li>
  <li>I'm thinking about turning this project into a "white-labelable" site for folks in academia - <a href="mailto:johnmarianhoffman@gmail.com">interested?</a></li>
</ul>

<p>This website and the links on the home page are the only web presences I maintain.  Although they are not <em>broadly</em> intended for the public (mostly friends and family), I also have an <a href="https://www.instagram.com/jmh_woodworks/">instagram</a>, a few videos on <a href="https://www.youtube.com/@johnmarianhoffman">youtube</a>, and a largely inactive <a href="https://www.linkedin.com/in/john-hoffman-77103b11">linkedin</a>.  It's 2023, and many of us maintain fairly separate personal/professional lives online so please use your best judgement about whether or not you wish to follow me.  Not everything online is meant for everyone, right?</p>

<p>I mention the above to also say that in an era of fake accounts, and AI generated whatnot, etc. these are the <em>only</em> online places to find me.</p>

<p>Thanks for taking the time to click this link and I'd love to hear from you!  Any questions or comments can be directed to <a href="mailto:johnmarianhoffman@gmail.com">johnmarianhoffman@gmail.com</a>.</p>

<p style="text-align:right;">John - July 16, 2023</p>
{{end}}