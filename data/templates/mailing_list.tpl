{{define "content"}}

<div class="container mx-auto mt-auto mb-auto" style="width:300px">
	<div class="row">
		<div class="col-4">
			<img src="{{.FormImage}}" class="mx-auto d-block rounded" width="300px">
		</div>
	</div>
</div>

<div class="row">
	<div class="col">
		<p style="text-align:center;">{{.FormNote}}</p>
	</div>
</div>

<div class="container mx-auto mt-auto mb-auto" style="width:400px">
	<div class="row">
		<div class="col" align="center">
			<form onsubmit="submit_form(event)">
				<div class="form-group">
					<label for="input-email">Email:</label>
					<input type="email" class="form-control" id="input-email" name="input-email" aria-describedby="emailHelp" placeholder="your email" required>
					<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
				</div>
				<div class="col" align="center">
					<div class="form-group mx-auto mt-auto">
						<button class="btn btn-primary" type="submit">sign up!</button>
					</div>
				</div>

				<div id="alert-placeholder" class="alert" role="alert"></div>
				
			</form>
		</div>
	</div>

	<script>
	 function submit_form(event) {

	   event.preventDefault()

	   console.log("submit form called!")

	   let input_email = document.getElementById("input-email").value

	   console.log(input_email)
	   
	   let form_data = new FormData()
	   form_data.append("email", input_email)
	   
	   console.log(form_data)
	   console.log(JSON.stringify(form_data))
	   
	   fetch("/mailing/{{.FormName}}", {
		 method: "POST",
		 headers: {
		   'Accept': 'application/json',
		   'Content-Type': 'application/json',
		 },
		 body: JSON.stringify({"email": input_email}),
	   }).then((response) => {

		 let alert = document.getElementById("alert-placeholder")
		 
		 if (!response.ok) {
		   alert.innerHTML = "error adding to mailing list (please check back later)"
		   alert.classList.add("alert-danger")
		 } else {		   
		   alert.innerHTML = "success! (redirecting in 5 seconds)"
		   alert.classList.add("alert-success")
		 }

		 console.log(response)
		 
		 // Redirect to target page
		 setTimeout(() => {window.location.replace("{{.FormRedirect}}")}, 5000);

	   })

	   return false
	 }
	</script>


{{end}}