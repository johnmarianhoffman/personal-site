<!-- TITLE TEMPLATE -->
{{define "title"}}
<hr>
<div class="container" style="padding:20px">
	<!-- <div class="row">
		 <div class="col">
		 {{with .PersonalInfo}}
		 <h1 style="text-align:center;">{{.FirstName}} {{.LastName}}</h1>
		 {{end}}
		 </div>
		 </div> -->
	
	<div class="row" style="align-items:center;">

		{{with .PersonalInfo}}

		<div id="photo-email" class="col-md-6">
			<img src="{{.ProfilePhoto}}" height="auto" class="w-100" style="border-radius:25px;" alt="profile photo of john working at his computer(s)" ></img>
		</div>		
		{{end}}

		{{with .Landing}}
		<div id="hello-note" class="col-md-6">
			<h2 style="text-align:center;padding:0.5em;">{{.Title}}</h2>
			<p>{{.Message}}</p>
			{{end}}
			{{with .PersonalInfo}}
			<p style="text-align:center;">{{.Email}}</p>
			{{end}}
		</div>
		
	</div>
</div>
<hr>
{{end}}

<!-- LANDING TEMPLATE -->
{{define "landing"}}
{{end}}

<!-- LISTBOX TEMPLATE -->
{{define "list-box"}}
<h2>{{.Title}}</h2>
<ul>
	{{range .Items}}
	<li>{{.ToHTML}}</li> 
	{{end}}
</ul>
{{end}}

<!-- RESEARCH INTERESTS TEMPLATE -->
{{define "research-interests"}}
<h2>Research Interests</h2>
<p>{{.}}</p>
<h4>Computing</h4>
<p>I personally am deeply interested in the computational details of all of the problems I work on and encourage my students to also dive deep to better understand these details.   Computation lies at the core of modern medicine in every way (PACS, image viewers, scheduling, drug delivery, security of a hospital, CT image reconstruction, CT image acquisition, etc.) and impact not only the research that we do, but the real viability of certain techniques both for research and clinic.  As with all science, through a better understanding our tools we gain access to new knowledge.  My work has included GPU algorithm implementation with OpenCL and CUDA, CPU architecture-specific optimization, deployment of algorithms as services (server/backend deployment), low-level network programming for distributed computing, cloud development and deployment, and many other topics.  I love seeing projects move from conception to deployment and start being used by real people.  The hacker mentality of "crack it open and figure out how it works" runs deep.</p>
{{end}}

{{define "content"}}
<div class="container">
	
	{{template "title" .}}
	
	{{template "research-interests" .PersonalInfo.ResearchInterests}}
	<hr>
	{{template "list-box" .Websites}}
	<hr>
	{{template "list-box" .Degrees}}
	<hr>
	{{template "list-box" .Jobs}}
	<hr>
	{{template "list-box" .Publications}}
	<hr>
	{{template "list-box" .Abstracts}}
	<hr>
</div>
{{end}}
