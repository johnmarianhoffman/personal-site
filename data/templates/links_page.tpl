{{define "content"}}

<h1 style="text-align:center;">Useful and Annotated Links</h1>

<p>Search engines really jumped the shark somewhere circa 2022-2023.  Maybe human curation can help a bit?  These are as much for me as they are for anyone stopping by.</p>
<p>I am not responsible for the content you find on the pages below, but felt like I wanted to keep it around for at least some reason. Also no guarantees the links won't break at some point.  I'll do my best to backup or mirror anything I think is extra important.</p>

<p style="text-align:right;">Last updated by John - 2025-01-21</p>
<div class="row">
  <div class="column">
    <h3>Image reconstruction</h3>
    <ul > 

      <li><a href="https://visielab.uantwerpen.be/astra-toolbox">Astra Toolbox</a></li>
      <p class="item compact">Astra Toolbox looks to be a high quality toolkit for tomographic operations</p>

      <li><a href="https://gitlab.com/freect/freect"><b>FreeCT</b></a></li>
      <p class="item compact">FreeCT is our lab's CT image reconstruction software.</p>

      <li><a href="https://sites.duke.edu/qial/mcr-toolkit/">MCR Toolkit</a></li>
      <p class="item compact">Multi-channel CT reconstruction toolkit from Duke</p>


    </ul>
  </div>

  <div class="column">
    <h3>Nice tools</h3>
    <ul > 
      <li><a href="https://asciiflow.com">Ascii Flow</a></li>
      <p class="item compact">ascii diagramming tool; useful for embedding in text documentation</p>
    </ul>
  </div>
</div>

<div class="row">
  <div class="column">
    <h3>Scientific practice</h3>
    <ul> 
      <li><a href="https://vita.had.co.nz/papers/tidy-data.pdf">Tidy Data</a></li>
    </ul>
  </div>

  <div class="column">
  </div>

</div>

{{end}}
