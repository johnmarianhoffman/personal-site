{{define "content"}}
<div class="container">
	
    <h1 style="text-align:center;">"Nuggets"</h1>

	<p>This space is for ideas that are not ready or not well-formed enough for a publication, but for whatever reason felt fun or important to share.</p>

	<p>Please keep in mind: I'm just some guy on the internet.  It's *possible* that I'm wrong or my thinking has evolved since the time of writing.  This writing is to work through ideas, learn, and grow.</p>

	<p>The name comes from a deeply influential mentor of mine, <a href="https://www1.phys.vt.edu/~rkpzia/HmPg.html">Royce Zia</a>.  I always thought the name "nuggets" was funny.</p>

	<hr>

	<div class="mx-auto d-flex flex-column" style="max-width:900px;">

		<table>
			<colgroup>
				<col span="1" style="width: 5%;">
				<col span="1" style="width: 75%;">
				<col span="1" style="width: 20%;">
			</colgroup>
			
			{{range .Display}}
			
			<tr>
				<td style="height: 40px;"><a href="nugget/{{.PermalinkStub}}" onmouseover="set_icon(this, {{.LinkIconHover}})" onmouseout="set_icon(this, {{.LinkIcon}})">{{.LinkIcon}}</a></td>
				<!-- <td><a href="javascript: void(0)" onclick="toggle_visibility('{{.CreatedAt}}')"><b>{{.Title}}</b></a></td> -->
				<td style="height: 40px;"><a href="nugget/{{.PermalinkStub}}"><b>{{.Title}}</b></a></td>
				<td style="height: 40px;"><a style="text-align:right;"><em>{{.CreatedAt}}</em></a></td>
			</tr>

			<!-- <div id="{{.CreatedAt}}" style={{ if .Visible }}"display:block;"{{ else }}"display:none;"{{ end }}>
				 {{.InnerHTML}}
				 <p style="text-align:center;">{{.Author}} - <a href="/nugget/{{.PermalinkStub}}">permalink</a></p>
				 </div> -->
			
			{{end}}
		</table>
	</div>
	
</div>

<script>
 function toggle_visibility(id) {
   var x = document.getElementById(id);
   if (x.style.display === "none") {
	 x.style.display = "block";
   } else {
	 x.style.display = "none";
   }
 }

 function set_icon(x, icon) {
   x.innerHTML = icon
 }

</script>
{{end}}