{{define "content"}}

{{with .Nugget}}
<div class="container">

	<div class="row">
		<div class="col">
			<h1 style="text-align:center;">{{.Title}}</h1>
		</div>
	</div>

	{{.InnerHTML}}

	<div class="row">
		<div class="col">
			<p style="text-align:center;">{{.Author}} - {{.CreatedAt}}</p>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<p style="text-align:center;">tags: {{range $key, $value := .Tags}}<a href="/nuggets?tags={{$key}}">{{$key}}</a> {{end}}</p>
		</div>
	</div>
	
</div>

{{end}}

{{end}}