{{define "content"}}
<div class="container">
  <form method="post" action="/_/{{.Type}}/{{.ID}}">
    <h1>Update {{.Type}}</h1>
    {{inputs_for .Item}}
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
{{end}}