{{define "base"}}
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="initial-scale=1">
		<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
		<link rel="stylesheet" href="/static/bootstrap.min.css">
		<link rel="icon" type="image/x-icon" href="/static/favicon.ico">
    <style>

    .hiddendesc .item {
      max-height: 0;
      transition: max-height 0.15s ease-out;
      overflow: hidden;
      background: #d5d5d5;
    }

    .hiddendesc:hover .item {
      max-height: 500px;
      transition: max-height 0.25s ease-in;
    }

    .compact {
      padding: 0;
      margin-top: 0px;
      margin-right: 0px;
      margin-left: 0px;
      margin-bottom: 5px;
    }

    </style>

		<!-- opengraph stuff -->
		<meta property="og:title" content={{.TemplateContext.OGTitle}} />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="{{.TemplateContext.OGLink}}" />
		<meta property="og:image" content="{{.TemplateContext.OGImage}}"/>
		<meta property="og:image:width" content="250"/>
		<meta property="og:image:alt" content="{{.TemplateContext.OGImageAlt}}"/>
		<meta property="og:description" content="{{.TemplateContext.OGDescription}}"/>
		<meta property="og:site_name" content="johnmarianhoffman.com"/>
		<meta property="og:locale" content="en_US" />
	</head>
	<body class="d-flex flex-column min-vh-100">

		{{with .TemplateContext}}
		<div class="container" {{if .HideNav}}style="display:none;"{{end}}>
			<nav style="text-align:center;padding:20px">
				<a href="/">home</a> | 
				<a href="/nuggets">nuggets</a> |
				<a href="/links">links</a> |
				<a href="/about">about</a> {{if .SessionValid}}|
				<a href="/_/dash">dash</a> |
				<a href="/_/logout">logout</a>{{end}}
			</nav>
		</div>
		{{end}}
		
		<div class="container">
			{{template "content" .}}
		</div>

		{{if .TemplateContext.HideFooter}}
			{{else}}
			<footer class="mt-auto">
				<hr>
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="text-center" style="font-size:0.75em"> All content copyright © John Hoffman 2020-2024.  Site content licensed under the <a href="https://creativecommons.org/licenses/by-nc/4.0/">CC BY-NC 4.0</a>.  I do geo-lookup on visiting IPs to get a rough idea of who's coming by.  I don't use any cookies or store any information about you.  Bring back the old web! ❤️</div>
						</div>
					</div>
				</div>
				<br>
			</footer>
			{{end}}
	</body>
</html>
{{end}}
