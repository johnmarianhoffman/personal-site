package main

type RequestCounter struct {
	count int
	c chan struct{}
}

func NewRequestCounter() *RequestCounter {

	req_counter := &RequestCounter{
		c: make(chan struct{}, 1),
		count: 0,
	}

	// Goroutine to monitor for thread safe incrementing
	go func() {
		for range req_counter.c {
			req_counter.count++
		}
	}()
	
	return req_counter
}

func (r *RequestCounter) Increment() {
	r.c <- struct{}{}
}

func (r *RequestCounter) Get() int {
	return r.count
}