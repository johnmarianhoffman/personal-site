package main

import (
	_ "embed"
	"flag"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

// Globals.  Defaults are for me.
// TODO: move these to something like viper
var (
	data_dir            = "data"
	private_dir         = filepath.Join(data_dir, "private")
	static_dir          = filepath.Join(data_dir, "static")
	media_dir           = filepath.Join(data_dir, "media")
	template_dir        = filepath.Join(data_dir, "templates")
	nugget_dir   string = "data/nuggets"
)

var (
	user = os.Getenv("USER")
	pass = os.Getenv("PASS")
)

var sessions *SessionStore = &SessionStore{}

var (
	startup_time time.Time
	version          = "v0.1.2"
	curr_cv      *CV = &CV{}
	templates        = make(map[string]*template.Template, 0)
)

var (
	nuggets         []Nugget = make([]Nugget, 0)
	request_counter          = NewRequestCounter()
)

var location_cache = NewMapCache()

var (
	disable_geolookup = false
	session_duration  string
)

var mailing_lists = map[string]*MailingList{
	"aapm-2024": NewMailingList(
		"aapm-2024",
		"You are subscribing to receive one (1) email when the spec for the HD phantom is available.  All data will be removed after that.",
		"/media/phantom-header-image.png",
		"/project/phantoms",
	),
}

func main() {
	flag.BoolVar(&disable_geolookup, "disable-geo", false, "disable geo lookup of ip address (useful for debugging)")
	flag.StringVar(&session_duration, "session-duration", "12h", "set the logged in session duration for the admin account. See `go doc time.ParseDuration`.")
	flag.Parse()

	if _, err := time.ParseDuration(session_duration); err != nil {
		log.Fatal("failed to parse session duration (see `go doc time.ParseDuration`)")
	}

	// This replaces an "init" function (which I often want to work around
	for idx := range mailing_lists {
		err := mailing_lists[idx].Load()
		if err != nil {
			log.Warn("failed to load mailing list: ", err)
		}
	}

	fmt.Println("eat by asodijfsoidfj: ", mailing_lists["aapm-2024"].Redirect)

	templates = LoadTemplates(template_dir)
	ScanForNuggets()

	startup_time = time.Now()

	err := curr_cv.Load()
	if err != nil {
		log.Warn("no existing db found:", err)

		curr_cv = DefaultCV()
	}

	r := mux.NewRouter()
	r.Use(GeolocationMiddleware)
	r.Use(LoggingMiddleware)
	r.Use(AuthMiddleware)

	// Public endpoints
	r.HandleFunc("/", IndexHandler)
	r.HandleFunc("/about", AboutHandler)
	r.HandleFunc("/links", LinksHandler)
	r.HandleFunc("/nuggets", NuggetsHandler)
	r.HandleFunc("/nugget/{id}", SingleNuggetHandler)
	r.HandleFunc("/login", LoginHandler)
	r.HandleFunc("/version", VersionHandler)
	r.HandleFunc("/project/{proj_name}", ProjectHandler)
	r.HandleFunc("/mailing/{list}", MailingListHandler)

	r.HandleFunc("/robots.txt", RobotsHandler)
	fs_static := http.FileServer(http.Dir(static_dir))
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fs_static))

	fs_media := http.FileServer(http.Dir(media_dir))
	r.PathPrefix("/media/").Handler(http.StripPrefix("/media/", fs_media))

	// Protected endpoints
	r_prot := r.PathPrefix("/_/").Subrouter()
	r_prot.Use(RedirectUnauthMiddleware)

	r_prot.HandleFunc("/upload", UploadHandler)
	r_prot.HandleFunc("/dash", AdminDashHandler)
	r_prot.HandleFunc("/publication/{id}", PublicationHandler)
	r_prot.HandleFunc("/abstract/{id}", AbstractHandler)
	r_prot.HandleFunc("/job/{id}", JobHandler)
	r_prot.HandleFunc("/website/{id}", WebsiteHandler)
	r_prot.HandleFunc("/logout", LogoutHandler)

	// Web hooks to trigger server actions
	r_prot.HandleFunc("/hooks/db/save", SaveDBHook)
	r_prot.HandleFunc("/hooks/nuggets/reload", ReloadNuggetsHook)
	r_prot.HandleFunc("/hooks/templates/reload", ReloadTemplatesHook)

	log.Fatal(http.ListenAndServe(":8080", r))
}
