package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	log "github.com/sirupsen/logrus"
)

func VersionHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "server version: %s", version)
}

func RobotsHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, `User-agent: GPTBot
Disallow: /
User-agent: ChatGPT-User
Disallow: /

User-agent: Amazonbot
User-agent: anthropic-ai
User-agent: Applebot-Extended
User-agent: Bytespider
User-agent: CCBot
User-agent: ChatGPT-User
User-agent: ClaudeBot
User-agent: Claude-Web
User-agent: cohere-ai
User-agent: Diffbot
User-agent: FacebookBot
User-agent: facebookexternalhit
User-agent: FriendlyCrawler
User-agent: Google-Extended
User-agent: GoogleOther
User-agent: GoogleOther-Image
User-agent: GoogleOther-Video
User-agent: GPTBot
User-agent: ICC-Crawler
User-agent: ImagesiftBot
User-agent: img2dataset
User-agent: Meta-ExternalAgent
User-agent: OAI-SearchBot
User-agent: omgili
User-agent: omgilibot
User-agent: PerplexityBot
User-agent: PetalBot
User-agent: Scrapy
User-agent: Timpibot
User-agent: VelenPublicWebCrawler
User-agent: YouBot
User-agent: Meta-ExternalFetcher
User-agent: Applebot
Disallow: /`)
}

func SingleNuggetHandler(w http.ResponseWriter, r *http.Request) {
	// w.WriteHeader(http.StatusOK)

	params := mux.Vars(r)
	nug_slug, ok := params["id"]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	nug := FindNuggetByStub(nug_slug)

	if nug == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	data := struct {
		TemplateContext
		Nugget *Nugget
	}{
		TemplateContext: TemplateContext{
			SessionValid: r.Context().Value("session_valid").(bool),
			OGTitle:      nug.Title,
			OGLink:       r.URL.String(),
			// OGDescription: "Nuggets are medium form writing on ideas not yet ready for publication",
			OGDescription: nug.Lede,
			OGImage:       "https://johnmarianhoffman.com/static/profile-photo.png",
		},
		Nugget: nug,
	}

	err := templates["nugget"].ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Error("failed to execute template: ", err)
	}
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	is_authed := r.Context().Value("session_valid").(bool)

	data := struct {
		TemplateContext TemplateContext
	}{
		TemplateContext: TemplateContext{
			SessionValid: is_authed,
			HideNav:      true,
		},
	}

	if is_authed {
		http.Redirect(w, r, "/_/dash", http.StatusFound)
		return
	}

	switch r.Method {
	case http.MethodGet:
		err := templates["login"].ExecuteTemplate(w, "base", data)
		if err != nil {
			log.Error("failed to execute template: ", err)
		}
	case http.MethodPost:
		r.ParseMultipartForm(4096)

		if r.FormValue("username") == user && r.FormValue("password") == pass {
			// Generate token, set cookie, save to sessions
			s := NewSession(r)
			err := sessions.Add(s)
			if err != nil {
				log.Warn(err.Error() + " (this could be bad behavior)")
				http.Error(w, "failed to create session", http.StatusConflict)
				return
			}

			cookie := &http.Cookie{Name: "session_token", Value: s.Token, Expires: s.Expiry}
			http.SetCookie(w, cookie)

			log.Info("authentication success!")
		}

		http.Redirect(w, r, "/_/dash", http.StatusFound)
	}
}

func AboutHandler(w http.ResponseWriter, r *http.Request) {
	session_valid := r.Context().Value("session_valid").(bool)

	data := struct {
		TemplateContext TemplateContext
	}{
		TemplateContext: TemplateContext{
			SessionValid:  session_valid,
			OGTitle:       "About johnmarianhoffman.com",
			OGLink:        r.URL.String(),
			OGDescription: "Read more about this site, why it exists, and how it works.",
			OGImage:       "https://johnmarianhoffman.com/static/profile-photo.png",
		},
	}

	err := templates["about"].ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Error("failed to execute template: ", err)
	}
}

func LinksHandler(w http.ResponseWriter, r *http.Request) {
	session_valid := r.Context().Value("session_valid").(bool)

	data := struct {
		TemplateContext TemplateContext
	}{
		TemplateContext: TemplateContext{
			SessionValid:  session_valid,
			OGTitle:       "Curated links - johnmarianhoffman.com",
			OGLink:        r.URL.String(),
			OGDescription: "",
			OGImage:       "https://johnmarianhoffman.com/static/profile-photo.png",
		},
	}

	err := templates["links"].ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Error("failed to execute template: ", err)
	}
}

func NuggetsHandler(w http.ResponseWriter, r *http.Request) {
	var display []Nugget

	// check for tag
	// currently only cleanly supports a single tag
	tag_str := r.URL.Query().Get("tags")
	if tag_str != "" {
		tags := strings.Split(tag_str, ",")

		for idx := range tags {
			display = FilterTag(tags[idx])
		}

	} else {
		display = nuggets
	}

	session_valid := r.Context().Value("session_valid").(bool)

	data := struct {
		TemplateContext
		Display []Nugget
	}{
		TemplateContext: TemplateContext{
			SessionValid:  session_valid,
			OGTitle:       "Nuggets - johnmarianhoffman.com",
			OGLink:        r.URL.String(),
			OGDescription: "Nuggets are medium form writing on ideas not yet ready for publication",
			OGImage:       "https://johnmarianhoffman.com/static/profile-photo.png",
		},
		Display: display,
	}

	err := templates["nuggets"].ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Error("failed to execute template: ", err)
	}
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	session_valid := r.Context().Value("session_valid").(bool)

	data := struct {
		*CV
		TemplateContext TemplateContext
	}{
		CV: curr_cv,
		TemplateContext: TemplateContext{
			SessionValid:  session_valid, // todo: session_valid
			OGTitle:       "John Marian Hoffman - CV",
			OGLink:        r.URL.String(),
			OGDescription: "I am an imaging scientist and computing researcher specializing in CT imaging and other forms of 3D reconstruction.",
			OGImage:       "https://johnmarianhoffman.com/static/profile-photo.png",
		},
		// SessionValid: false, // todo: session_valid
	}

	err := templates["index"].ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Error("failed to execute template: ", err)
	}
}

func ProjectHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	proj_name, ok := params["proj_name"]
	if !ok {
		log.Error("")
	}

	log.Info(proj_name)

	p, err := LoadProject(proj_name)
	if err != nil {
		http.Error(w, "project not found: "+err.Error(), http.StatusNotFound)
		return
	}

	data := struct {
		TemplateContext
		Project *Project
	}{
		TemplateContext: TemplateContext{
			OGTitle:       p.Title,
			OGLink:        r.URL.String(),
			OGDescription: p.Abstract,
			OGImage:       p.MainPhoto,
		},
		Project: p,
	}

	err = templates["project"].ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Error("failed to execute template: ", err)
	}
}

func PublicationHandler(w http.ResponseWriter, r *http.Request) {
	UpdateHandler("publication", w, r)
}

func AbstractHandler(w http.ResponseWriter, r *http.Request) {
	UpdateHandler("abstract", w, r)
}

func JobHandler(w http.ResponseWriter, r *http.Request) {
	UpdateHandler("job", w, r)
}

func WebsiteHandler(w http.ResponseWriter, r *http.Request) {
	UpdateHandler("website", w, r)
}

func UpdateHandler(resource_type string, w http.ResponseWriter, r *http.Request) {
	// Check for id
	params := mux.Vars(r)
	id, ok := params["id"]
	if !ok {
		log.Error("")
	}

	// Grab current item
	var curr_item interface{}

	switch resource_type {
	case "abstract":
		// curr_item = curr_cv.Abstracts.Items[id]
	case "publication":
		// curr_item = curr_cv.Publications.Items[id]
	case "job":
		curr_item = curr_cv.Jobs.Items[id]
	case "website":
		curr_item = curr_cv.Websites.Items[id]
	}

	if curr_item == nil {
		switch resource_type {
		case "abstract":
			curr_item = &Abstract{}
		case "publication":
			curr_item = &Publication{}
		case "job":
			curr_item = &Job{}
		case "website":
			curr_item = &Website{}
		}
	}

	switch r.Method {
	case http.MethodPost:
		r.ParseForm()
		dec := schema.NewDecoder()
		dec.IgnoreUnknownKeys(true)

		err := dec.Decode(curr_item, r.PostForm)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		switch resource_type {
		case "abstract":
			// curr_cv.Abstracts.Items[id] = curr_item.(*Abstract)
		case "publication":
			// curr_cv.Publications.Items[id] = curr_item.(*Publication)
		case "job":
			curr_cv.Jobs.Items[id] = curr_item.(*Job)
		case "website":
			curr_cv.Websites.Items[id] = curr_item.(*Website)
		}

		err = curr_cv.Save()
		if err != nil {
			log.Error("during record update, failed to save db:", err)
		}

		fallthrough
	case http.MethodGet:

		data := struct {
			ID   string
			Type string
			Item interface{}
		}{
			ID:   id,
			Type: resource_type,
			Item: curr_item,
		}

		// err = tpl.Execute(w, data)
		err := templates["update"].ExecuteTemplate(w, "base", data)
		if err != nil {
			log.Error("failed to execute template: ", err)
		}
	case http.MethodDelete:
		switch resource_type {
		case "abstract":
			// delete(curr_cv.Abstracts.Items, id)
		case "publication":
			// delete(curr_cv.Publications.Items, id)
		case "job":
			delete(curr_cv.Jobs.Items, id)
		case "website":
			delete(curr_cv.Websites.Items, id)
		}

		err := curr_cv.Save()
		if err != nil {
			log.Error("during delete, failed to save db:", err)
		}
	}
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	session_cookie, err := r.Cookie("session_token")
	if err != nil {
		// todo better error
		return
	}

	sessions.Expire(session_cookie.Value)

	http.Redirect(w, r, "/", http.StatusFound)
}

func UploadHandler(w http.ResponseWriter, r *http.Request) {
	// Get multipart file and header parts
	mp_f, mp_h, err := r.FormFile("uploaded_file")
	if err != nil {
		log.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Create an output file
	target_idx := r.FormValue("target")

	target_dir := media_dir

	switch target_idx {
	case "0":
		target_dir = media_dir
	case "1":
		target_dir = static_dir
	case "2":
		target_dir = nugget_dir
	}

	f_out, err := os.Create(filepath.Join(target_dir, mp_h.Filename))
	if err != nil {
		log.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Read data into memory
	data, err := io.ReadAll(mp_f)
	if err != nil {
		log.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Write data out to file
	n_bytes, err := f_out.Write(data)
	if err != nil {
		log.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	log.Infof("read & wrote %d bytes", n_bytes)
}

func MailingListHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	list_name, ok := params["list"]
	if !ok {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	curr_list, ok := mailing_lists[list_name]
	if !ok {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	// Only supported list for now is AAPM 2024 - HD phantom signup
	if list_name != "aapm-2024" {
		// w.WriteHeader(http.StatusBadRequest)
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	// fmt.Println("==========> jijdi    ", curr_list.Redirect)

	switch r.Method {
	case http.MethodGet:

		data := struct {
			TemplateContext
			FormName     string
			FormNote     string
			FormImage    string
			FormRedirect string
		}{
			TemplateContext: TemplateContext{
				SessionValid: false,
				HideNav:      true,
				HideFooter:   true,
			},
			FormName:     curr_list.Name,
			FormNote:     curr_list.Note,
			FormImage:    curr_list.Image,
			FormRedirect: curr_list.Redirect,
		}

		err := templates["mailing"].ExecuteTemplate(w, "base", data)
		if err != nil {
			err_msg := "failed to update list (john's problem)" + err.Error()
			log.Error(err_msg)
			http.Error(w, err_msg, http.StatusInternalServerError)
			return
		}

	case http.MethodPost:

		le := &ListEntry{}

		err := json.NewDecoder(r.Body).Decode(le)
		if err != nil {
			err_msg := "bad submission (your problem): " + err.Error()
			log.Error(err_msg)
			http.Error(w, err_msg, http.StatusBadRequest)
		}

		curr_list.Insert(*le)

		err = curr_list.Save()
		if err != nil {
			err_msg := "bad submission (your problem): " + err.Error()
			http.Error(w, err_msg, http.StatusInternalServerError)
			return
		}

	}
}
