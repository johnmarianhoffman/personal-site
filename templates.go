package main

import (
	"html/template"
	"path/filepath"

	"github.com/joncalhoun/form"
	log "github.com/sirupsen/logrus"
)

type TemplateContext struct {
	SessionValid  bool
	HideNav       bool
	HideFooter    bool
	OGTitle       string
	OGLink        string
	OGImage       string
	OGImageAlt    string
	OGDescription string
}

func LoadTemplates(tpl_dir string) map[string]*template.Template {
	log.Info("reloading templates from " + tpl_dir)

	tpls := make(map[string]*template.Template)

	base := filepath.Join(tpl_dir, "base.tpl")
	index := filepath.Join(tpl_dir, "index.tpl")
	about := filepath.Join(tpl_dir, "about.tpl")
	links := filepath.Join(tpl_dir, "links_page.tpl")
	nugget := filepath.Join(tpl_dir, "nugget.tpl")
	nuggets := filepath.Join(tpl_dir, "nuggets.tpl")
	login := filepath.Join(tpl_dir, "login.tpl")
	update := filepath.Join(tpl_dir, "update.tpl")
	project := filepath.Join(tpl_dir, "project.tpl")
	mailing := filepath.Join(tpl_dir, "mailing_list.tpl")

	tpls["index"] = template.Must(template.ParseFiles(index, base))
	tpls["about"] = template.Must(template.ParseFiles(about, base))
	tpls["links"] = template.Must(template.ParseFiles(links, base))
	tpls["nugget"] = template.Must(template.ParseFiles(nugget, base))
	tpls["nuggets"] = template.Must(template.ParseFiles(nuggets, base))
	tpls["login"] = template.Must(template.ParseFiles(login, base))
	tpls["project"] = template.Must(template.ParseFiles(project, base))
	tpls["mailing"] = template.Must(template.ParseFiles(mailing, base))

	// Load the auto-form template (a little more complex)
	input_tpl := `
<div class="form-group">
<label {{with .ID}}for="{{.}}"{{end}}>
	            {{.Label}}
            </label>
            <input class="form-control" {{with .ID}}id="{{.}}"{{end}} type="{{.Type}}" name="{{.Name}}" placeholder="{{.Placeholder}}" {{with .Value}}value="{{.}}"{{end}}>
</div>
`

	tpl_input := template.Must(template.New("").Parse(input_tpl))

	fb := form.Builder{
		InputTemplate: tpl_input,
	}

	tpls["update"] = template.Must(template.New("update").Funcs(fb.FuncMap()).ParseFiles(update, base))

	return tpls
}
