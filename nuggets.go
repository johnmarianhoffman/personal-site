package main

import (
	"bufio"
	"html/template"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"

	
	log "github.com/sirupsen/logrus"
	"math/rand"
)

type Nugget struct {
	Title              string
	Summary            string // do not use.  only kept around for legacy reasons
	Lede               string
	Author             string
	CreatedAt          string
	TimeStamp          time.Time
	Path               string
	IsMarkdown         bool
	SimulationFilename string
	IsHidden           bool // will not be served
	Visible            bool // already unfurled when page is loaded

	Tags              TagSet
	InnerHTML         template.HTML
	ProcessedMarkdown string

	LinkIcon string
	LinkIconHover string

	PermalinkStub     string
}

// Random unicode arrow for link icons
// Is this too offensive?
func RandomArrow() rune {
	//a, b := int32(0x2190), int32(0x21bb)
	a, b := int32(0x2190), int32(0x21a0)
	return rune(rand.Int31n(b-a) + a)
}

// TODO! (sanity checking)
func ValidateStub(stub string) bool {
	return true 
}

// If no specific stub is provided, generate one from the title
func StubFromTitle(title string) string {

	re := regexp.MustCompile("[0-9/?,.:;']")

	title = re.ReplaceAllString(title, "") // remove all numbers
	title = strings.TrimSpace(title)
	
	// gotta be a better way to do this
	title = strings.ToLower(title)
	title = strings.Replace(title, " ", "-", -1) // No spaces
	return title
}

// If folks were doing a lot of lookups for permalinks, this would be inefficient.
// for now, this should be find for what I need.
func FindNuggetByStub(stub string) *Nugget {
	for idx := range nuggets {
		if nuggets[idx].PermalinkStub == stub {
			return &nuggets[idx]
		}
	}

	return nil
} 

// this may be a little memory intensive on a per-request basis.
// might want to precalculate lookup table on load?
func FilterTag(tag string) []Nugget {

	filter_set := make([]Nugget, 0)

	for idx := range nuggets {
		if nuggets[idx].Tags.Contains(tag) {
			filter_set = append(filter_set, nuggets[idx])
		}
	}

	return filter_set
}

// We assume that every file in nugget_dir is a nugget post (may get annoying)
func ScanForNuggets() {
	nuggets = []Nugget{}
	
	entries, _ := os.ReadDir(nugget_dir)

	for _, entry := range entries {
		nugget_path := filepath.Join(nugget_dir, entry.Name())
		nug := scanMarkdown(nugget_path)

		// Generate stubs for anything missing one
		if nug.PermalinkStub == "" {
			nug.PermalinkStub = StubFromTitle(nug.Title)
		}
		
		nuggets = append(nuggets, nug)
	}

	// Sort by most recent first
	sort.Slice(nuggets, func(i, j int) bool { return !nuggets[i].TimeStamp.Before(nuggets[j].TimeStamp) })
	
}

func MarkdownToHTML(md []byte) []byte {
	// create markdown parser with extensions
	//extensions := parser.CommonExtensions | parser.AutoHeadingIDs | parser.NoEmptyLineBeforeBlock
	extensions := parser.CommonExtensions
	p := parser.NewWithExtensions(extensions)
	doc := p.Parse(md)

	// create HTML renderer with extensions
	htmlFlags := html.CommonFlags | html.HrefTargetBlank
	opts := html.RendererOptions{Flags: htmlFlags}
	renderer := html.NewRenderer(opts)

	return markdown.Render(doc, renderer)
}

// scanMarkdown is my lightweight pre-parser to extract some of the
// site-specific stuff that I've implemented.  We're not actually parsing
// the markdown to HTML here.  Just essentially getting if off the disk
// and into memory, while stripping out some of the non-markdown info I'm
// using.
func scanMarkdown(filepath string) Nugget {

	log.Info("scanning markdown file ", filepath)

	n := Nugget{
		LinkIcon: string(RandomArrow()),
		LinkIconHover: string(RandomArrow()),
	}

	title_pattern := regexp.MustCompile(`# ([[:graph:]\s]+)`)	
	bang_pattern := regexp.MustCompile(`!([a-zA-Z_]+): (.*)$`)

	f, _ := os.Open(filepath) // must exist to be included here

	content := &strings.Builder{}
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {

		line := scanner.Text()

		// Empty line
		if line == "" {
			content.WriteString(line + "\n")
			continue
		}

		// Catch title
		// Note: we don't add the title to the markdown that will be rendered
		matches := title_pattern.FindAllStringSubmatch(line, 5)
		if matches != nil {
			n.Title = matches[0][1]
			continue
		}

		// Catch bangs (for date/author)
		// Note: Bang matches DO NOT get added to the final markdown content
		matches = bang_pattern.FindAllStringSubmatch(line, 5)
		if matches != nil {
			
			bang_key := strings.ToLower(matches[0][1])
			bang_value := matches[0][2]

			switch bang_key {
			case "lede":
				n.Lede = bang_value
				log.Warn("(remove this warning in future) lede found: ", n.Lede)
				continue
			case "tags":
				tags := strings.Split(bang_value, ",")
				n.Tags = NewTagSet(tags)
				continue
			case "date":
				raw_date := bang_value
				d, err := time.Parse("2006-01-02", raw_date)
				if err != nil {
					log.Fatal("invalid date: ", raw_date, ": ", err)
				}

				n.TimeStamp = d
				n.CreatedAt = d.Format("2006-01-02")
				continue
			case "author":
				n.Author = matches[0][2]
				continue
			case "hidden":
				switch strings.ToLower(bang_value) {
				case "true":
					n.IsHidden = true
				case "false":
					n.IsHidden = false
				case "1":
					n.IsHidden = true
				case "0":
					n.IsHidden = false
				default:
					log.Fatal("invalid value for !hidden: ", bang_value)
				}
				continue
			case "markdown":
				switch strings.ToLower(bang_value) {
				case "true":
					n.IsMarkdown = true
				case "false":
					n.IsMarkdown = false
				case "1":
					n.IsMarkdown = true
				case "0":
					n.IsMarkdown = false
				default:
					log.Fatal("invalid value for !markdown: ", bang_value)
				}
				continue
			case "created":
				t, err := time.Parse("2006-01-02", bang_value)
				if err != nil {
					log.Fatal("created bang value must be formatted YYYY-MM-DD")
				}
				n.TimeStamp = t
				n.CreatedAt = bang_value
				continue
			case "simulation":
				continue
			case "summary":
				n.Summary = bang_value
				continue

			case "stub":
				n.PermalinkStub = bang_value
				continue
			default:
				log.Fatal("unsupported !bang in nuggets markdown: ", bang_key, bang_value)
			}
		}

		content.WriteString(line + "\n")

	}

	n.ProcessedMarkdown = content.String()
	n.InnerHTML = template.HTML(string(MarkdownToHTML([]byte(n.ProcessedMarkdown)))) // ugly

	//fmt.Printf("%+v\n", n)

	return n
}
