package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

// IPLocation is used to support the IPLocationCache.
type IPLocation struct {
	IP        string
	Location  string
	CreatedAt time.Time
	Expiry    time.Time
}

// GeoLookup IP performs an API call to ip-api.com/json/$addr
// Even though this can fail, we dont pass the error up the call stack
// because we don't need to handle it anywhere beyond here atm.  We just
// need to set the location string of the session to something.
func GeoLookupIP(addr string) string {
	if disable_geolookup {
		return "ip location disabled with --disable-geo"
	}

	resp, err := http.Get("http://ip-api.com/json/" + addr)
	if err != nil {
		return "location error:" + err.Error()
	}

	ip_res := &IPResult{}
	err = json.NewDecoder(resp.Body).Decode(ip_res)
	if err != nil {
		msg := "failed to decode: " + err.Error()
		log.Error(msg)
		return msg
	}

	if ip_res.Status != "success" {
		log.Warn("geo-ip api error: ", ip_res.Status)
	}

	location_str := fmt.Sprintf("%s, %s (%s)", ip_res.City, ip_res.Region, ip_res.Country)

	return location_str
}

// IPLocationCache provides a minimal caching system
// with a single method: Lookup.
type IPLocationCache interface {
	Lookup(addr string) (location string)
}

type MapCache map[string]IPLocation

// Create a new IPLocationCache ready for use.
func NewMapCache() MapCache {
	return MapCache(make(map[string]IPLocation))
}

// Lookup returns the cached IP.  If the IP is not found in the map,
// it performs the lookup and caches the response.
func (c MapCache) Lookup(addr string) (location string) {
	curr_time := time.Now()
	duration := 12 * time.Hour

	addr = strings.Split(addr, ":")[0]

	_, exists := c[addr]
	if exists {
		location = c[addr].Location
		return
	}

	// Add the new IPLocation to the cache
	c[addr] = IPLocation{
		IP:        addr,
		Location:  GeoLookupIP(addr),
		CreatedAt: curr_time,
		Expiry:    curr_time.Add(duration),
	}

	// Dispatch the goroutine that will eventually clean up the cached item.
	// We could end up with a weird amount of goroutines running... maybe?
	// No concerning ourselves with proper cleanup.
	go func() {
		<-time.After(duration)

		// Sleep to ensure time.Now()>Expiry (a little klugey, may not be needed)
		time.Sleep(1 * time.Millisecond)

		// If the expiry has been updated, bail out
		if time.Now().Before(c[addr].Expiry) {
			return
		}

		// Clean up the cached entry
		delete(c, addr)
	}()

	return c[addr].Location
}
