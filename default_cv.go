package main

var Me Author = Author{First: "John", Last: "Hoffman"}
var Mike Author = Author{First: "Michael", Last: "McNitt-Gray"}
var Fred Author = Author{First: "Frédéric", Last: "Noo"}
var Grace Author = Author{First: "Grace", Last: "Hyun J. Kim"}
var Matt Author = Author{First: "Matthew", Last: "Brown"}
var Jonathan Author = Author{First: "Jonathan", Last: "Goldin"}
var Nastaran Author = Author{First: "Nastaran", Last: "Emaminejad"}
var Wasil Author = Author{First: "M. Wasil", Last: "Wahi-Anwar"}
var Stefano Author = Author{First: "Stefano", Last: "Young"}
var Scott Author = Author{First: "Scott", Last: "Hsieh"}

func DefaultCV() *CV {
	return &CV{
		PersonalInfo: PersonalInfo{
			FirstName:         "John",
			LastName:          "Hoffman",
			Email:             "johnmarianhoffman@gmail.com",
			ResearchInterests: "My research interests include 3D image and surface reconstruction, quantitative imaging and computer vision, and computational geometry. Additionally, I aim to incorporate automation, high-performance, and parallel computing into my research workflows to accelerate the amount of data we are able to consider and generate.",
			ProfilePhoto:      "/static/profile-photo.png",
		},
		Landing: Landing{
			Title: "Hello!",
			Message: `This website is largely an effort to archive and keep a record of my work, as well as a sort of business card for my experience. Please feel free to reach out if you'd like to collaborate or hire me for consulting work.

I'm currently rewriting my website from scratch, and not all of the abstracts, publications, and websites are currently available.  I will be migrating the data from the old site to the current one over the coming weeks."`,
			Initials: "JMH",
			Date:     "July 16, 2023",
		},
		Websites: WebsiteList{
			Title: "Websites",
			Items: map[string]*Website{
				"1": &Website{Title: "FreeCT", HREF: "https://gitlab.com/freect/freect"},
				"2": &Website{Title: "Personal GitLab", HREF: "https://gitlab.com/johnmarianhoffman/"},
				"3": &Website{Title: "Simulations", HREF: "https://simulations.johnmarianhoffman.com"},
				"4": &Website{Title: "Three Sentences", HREF: "https://threesentences.johnmarianhoffman.com"},
				"5": &Website{Title: "Nuggets (blog?)", HREF: "/nuggets"},
				"6": &Website{Title: "About this site", HREF: "/about"},
			},
		},
		Degrees: DegreeList{
			Title: "Academics",
			Items: map[string]*Degree{
				"1": &Degree{Type: DegreePhD, Major: "Biomedical Physics", University: "UCLA", City: "Los Angeles", State: "CA", Year: 2018, LinkTitle: "dissertation", LinkHREF: "https://escholarship.org/uc/item/6871m7kj"},
				"2": &Degree{Type: DegreeBS, Major: "Physics", University: "Virginia Tech", City: "Blacksburg", State: "VA", Year: 2011},
				"3": &Degree{Type: DegreeBS, Major: "Mathematics", University: "Virginia Tech", City: "Blacksburg", State: "VA", Year: 2011},
			},
		},
		Jobs: JobList{
			Title: "Job Experience",
			Items: map[string]*Job{
				"1": &Job{Title: "Assistant Adjunct Professor", Company: "UCLA Radiological Sciences", StartDate: "May 2022", EndDate: "Present"},
				"2": &Job{Title: "Lead Software Development Engineer", Company: "Magic Leap", StartDate: "April 2022", EndDate: "February 2022"},
				"3": &Job{Title: "Computer Vision Engineer, 3D Reconstruction", Company: "Magic Leap", StartDate: "December 2018", EndDate: "April 2021"},
				"4": &Job{Title: "Independent Contractor", Company: "iTomography", StartDate: "December 2017", EndDate: "May 2019"},
				"5": &Job{Title: "Imaging Scientist", Company: "Toshiba (now Canon) Medical Research USA, Inc.", StartDate: "October 2016", EndDate: "December 2017"},
			},
		},
		Publications: PublicationList{
			Title: "Selected Publications",
			Items: []*Publication{
				&Publication{
					Authors:  []Author{Scott, Me, Fred},
					Title:    "Accelerating Iterative Coordinate Descent Using a Stored System Matrix",
					Journal:  "Medical Physics",
					Year:     2019,
					Volume:   46,
					Issue:    12,
					Pages:    "e801-e809",
					LinkHREF: "https://aapm.onlinelibrary.wiley.com/doi/10.1002/mp.13543",
					PDFHREF:  "/media/accelerating-icd-stored-system-matrix.pdf",
				},
				&Publication{
					Authors:  []Author{Me, Nastaran, Wasil, Grace, Matt, Stefano, Mike},
					Title:    "Technical Note: Design and implementation of a high‐throughput pipeline for reconstruction and quantitative analysis of CT image data",
					Journal:  "Medical Physics",
					Volume:   46,
					Issue:    5,
					Pages:    "2310-2322",
					Year:     2019,
					LinkHREF: "https://aapm.onlinelibrary.wiley.com/doi/full/10.1002/mp.13401?af=R",
					PDFHREF:  "/media/design-and-implementation-of-a-high-throughput.pdf",
				},
				&Publication{
					Authors:  []Author{Me, Fred, Stefano, Scott, Mike},
					Title:    "Technical Note: FreeCT_ICD: An open-source implementation of a model-based iterative reconstruction method using coordinate descent optimization for CT imaging investigations",
					Journal:  "Medical Physics",
					Year:     2019,
					Volume:   45,
					Issue:    8,
					Pages:    "3591-3603",
					LinkHREF: "https://aapm.onlinelibrary.wiley.com/doi/10.1002/mp.13026",
					PDFHREF:  "/media/freect-icd.pdf",
				},
				&Publication{
					Authors:  []Author{Author{First: "Thomas", Last: "Martin"}, Me, Author{First: "Jeffrey", Last: "Alger"}, Mike, Author{First: "Danny", Last: "Wang"}},
					Title:    "Low‐dose CT perfusion with projection view sharing",
					Journal:  "Medical Physics",
					Year:     2017,
					Volume:   45,
					Issue:    1,
					Pages:    "101-113",
					LinkHREF: "https://aapm.onlinelibrary.wiley.com/doi/abs/10.1002/mp.12640",
					PDFHREF:  "media/ct-kwic.pdf",
				},
				&Publication{
					Authors:  []Author{Me, Stefano, Fred, Mike},
					Title:    "Technical Note: FreeCT_wFBP: A robust, efficient, open‐source implementation of weighted filtered backprojection for helical, fan‐beam CT",
					Journal:  "Medical Physics",
					Year:     2016,
					Volume:   43,
					Issue:    3,
					Pages:    "1411-1420",
					LinkHREF: "https://aapm.onlinelibrary.wiley.com/doi/abs/10.1118/1.4941953",
					PDFHREF:  "/media/freect-wfbp.pdf",
				},
			},
		},
		Abstracts: AbstractList{
			Title: "Selected Abstracts",
			Items: []*Abstract{
				&Abstract{
					Authors:    []Author{Me, Fred, Mike},
					Title:      "An analytic, physics-based approach to scoring emphysema in lung CT patients",
					Conference: "SPIE Medical Imaging",
					Location:   "San Diego, CA",
					Date:       "February 2023",
					LinkHREF:   "https://spie.org/medical-imaging/presentation/An-analytic-physics-based-approach-to-scoring-emphysema-in-lung/12465-108",
				},
				&Abstract{
					Authors:    []Author{Me, Scott, Fred, Mike},
					Title:      "FreeCT ICD: Free, Open-Source MBIR Reconstruction Software for Diagnostic CT",
					Conference: "The Fifth International Conference on Image Formation in X-Ray Computed Tomography",
					Location:   "Salt Lake City, UT",
					Date:       "May 2018",
				},
				&Abstract{
					Authors:    []Author{Me, Fred, Mike},
					Title:      "Influence of tube current modulation on Noise Statistics of reconstructed images in low-dose lung cancer CT screening",
					Conference: "AAPM Annual Meeting",
					Location:   "Denver, CO",
					Date:       "August 2017",
				},
				&Abstract{
					Authors:    []Author{Me, Grace, Jonathan, Matt, Mike},
					Title:      "A Pilot Study Evaluating the Robustness of Density Mask Scoring (RA-950), a Quantitative Measure of Chronic Obstructive Pulmonary Disease, to CT Parameter Selection Using a High-Throughput, Automated, Computational Research Pipeline",
					Conference: "AAPM Annual Meeting",
					Location:   "Denver, CO",
					Date:       "August 2017",
				},
				&Abstract{
					Authors:    []Author{Me, Fred, Stefano, Mike},
					Title:      "Tailoring TCM Schemes to a Task: Evaluating the Impact of Customized TCM Profiles on Detection of Lung Nodules in Simulated CT Lung Cancer Screening",
					Conference: "AAPM Annual Meeting",
					Location:   "Washington, DC",
					Date:       "August 2016",
				},
			},
		},
	}
}
