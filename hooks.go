package main

import (
	"net/http"
	log "github.com/sirupsen/logrus"
)

func ReloadTemplatesHook(w http.ResponseWriter, r *http.Request) {
	templates = LoadTemplates(template_dir)
}

func SaveDBHook(w http.ResponseWriter, r *http.Request) {
	err := curr_cv.Save()
	if err != nil {
		log.Error("failed to save db:", err)
		http.Error(w, "failed to save db:", http.StatusInternalServerError)
	}
}

func ReloadNuggetsHook(w http.ResponseWriter, r *http.Request) {
	ScanForNuggets()
}

