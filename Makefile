sync:
	rsync -rv ~/Code/personal-site \
		john@malokingi:~/

# TODO: Find a solution that is less bad
# Docker-only solution copying data into remote docker 
# volume would probably be the most "correct"
deploy:
	rsync -arvh * ts-malokingi:~/personal-site/ --delete --exclude=".git"

