package main

import (
	"testing"
	//"github.com/matryer/is"
	"fmt"
)

func TestStubGeneration(t *testing.T) {

	titles := []string{
		"Dabbling in Lattice-Lotka-Volterra Simulations",
		"1/?: GUI Programming in Science: The Main Loop, Events, and Callbacks",
		"Nonteological Thinking",
	}

	expected := []string{
		"dabbling-in-lattice-lotka-volterra-simulations",
		"gui-programming-in-science-the-main-loop-events-and-callbacks",
		"nonteological-thinking",
	}

	for idx, curr_title := range titles {
		stub := StubFromTitle(curr_title)

		fmt.Println(stub)

		//is.Equal(stub, expected[idx])
		
		if stub != expected[idx] {
			t.Errorf("%s != %s", stub, expected[idx])
		}
		
	}
}
