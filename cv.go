package main

import (
	"encoding/json"
	//"fmt"
	log "github.com/sirupsen/logrus"
	"os"
)

type CV struct {
	PersonalInfo PersonalInfo
	Landing      Landing
	Websites     WebsiteList
	Degrees      DegreeList
	Jobs         JobList
	Abstracts    AbstractList
	Publications PublicationList
}

func (cv *CV) Save() error {

	log.Info("saving db to data/db.json...")

	f, err := os.Create("data/db.json")
	if err != nil {
		return err
	}

	data, err := json.MarshalIndent(&cv, "", "  ")
	if err != nil {
		return err
	}

	_, err = f.Write(data)
	if err != nil {
		return err
	}

	return nil
}

func (cv *CV) Load() error {

	log.Info("loading db from data/db.json...")

	f, err := os.Open("data/db.json")
	if err != nil {
		return err
	}

	err = json.NewDecoder(f).Decode(cv)
	if err != nil {
		return err
	}

	return nil
}
