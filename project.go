package main

import (
	"html/template"
	"io"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v3"
)

type Project struct {
	Title            string
	Contributors     []*Author
	Abstract         string
	Body             template.HTML // must be html, at minimum wrapped in <p>
	RelevantLinks    []string
	Citations        []string // can be html for linking
	AdditionalPhotos []string
	MainPhoto        string
	MainCaption      string
}

var project_dir string = "data/projects"

// Ugh. How to handle metadata?  Embed into an html doc.
func LoadProject(proj_name string) (*Project, error) {

	// Project's must have <project-name>.yaml
	//
	// Body can be loaded from a separate file if desired, or
	// written directly into the yaml file.
	fp := filepath.Join(project_dir, proj_name+".yaml")

	f, err := os.Open(fp)
	if err != nil {
		return nil, err
	}

	p := &Project{}
	err = yaml.NewDecoder(f).Decode(p)
	if err != nil {
		return nil, err
	}

	_, err = os.Stat(filepath.Join(project_dir, string(p.Body)))
	if err == nil {
		body_path := filepath.Join(project_dir, string(p.Body))

		f, err := os.Open(body_path)
		if err != nil {
			return nil, err
		}

		buff, err := io.ReadAll(f)
		if err != nil {
			return nil, err
		}

		p.Body = template.HTML(string(buff))
	}

	return p, nil

}
