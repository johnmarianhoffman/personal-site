module gitlab.com/johnmarianhoffman/personal-site

go 1.18

require (
	github.com/gomarkdown/markdown v0.0.0-20230716120725-531d2d74bc12
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/joncalhoun/form v1.0.1
	github.com/sirupsen/logrus v1.9.3
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)

require golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
