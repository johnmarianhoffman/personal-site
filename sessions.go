package main

import (
	"github.com/google/uuid"
	"time"
	"net/http"
	//log "github.com/sirupsen/logrus"
	//"fmt"
	//"github.com/sirupsen/logrus"
	"errors"
)

type Session struct {
	Addr string
	Location string
	Token  string
	Expiry time.Time
	Expired bool
}

type IPResult struct {
	Query string
	Status string
	Country string
	Region string
	City string
}

func NewSession(r *http.Request) *Session {

	remote_addr := r.Context().Value("remote_addr").(string)

	// We have already check for errors on server startup
	dur, _ := time.ParseDuration(session_duration)
	
	s := &Session{
		Addr: remote_addr,
		Location: location_cache.Lookup(remote_addr),
		Token:  uuid.New().String(),
		Expiry: time.Now().Add(dur),
		Expired: false,
	}

	return s
}

type SessionStore map[string]*Session

func (sess SessionStore) IsValid(token string) bool {

	s, ok := sess[token]
	if !ok {
		return false
	}

	if s.Expired {
		return false
	}

	if time.Now().After(s.Expiry) {
		delete(sess, s.Token)
		return false
	}

	return true
}

func (sess SessionStore) Add(s *Session) error {

	// Check for someone trying to add an existing token (replay
	// attack or something?)
	if _, exists := sess[s.Token]; exists {
		err := errors.New("session already exists: " + s.Token)
		return err
		//log.Warn(err_msg + " (this could be bad behavior)")
		//http.Error(w, err_msg, http.StatusConflict)
	}

	sess[s.Token] = s

	// goroutine to expire session after duration
	go func(){

		time_to_expire := time.Until(s.Expiry)

		<-time.After(time_to_expire)

		// Sleep to ensure time.Now()>Expiry (a little klugey, may not be needed)
		time.Sleep(1*time.Millisecond)

		// If the expiry has been updated, bail out
		if time.Now().Before(sess[s.Token].Expiry) { return }
		
		sess.Expire(s.Token)
	}()

	return nil
}

func (sess SessionStore) Expire(sess_id string) {
	sess[sess_id].Expired = true
}


