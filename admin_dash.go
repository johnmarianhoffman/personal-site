package main

import (
	_ "embed"
	"html/template"
	"net/http"
	log "github.com/sirupsen/logrus"
	"os"
)

var base_template = "data/templates/base.tpl"
var dash_template = "data/templates/dash.tpl"

func listDir(dir string) []string {
	entries, err := os.ReadDir(dir)
	if err != nil {
		return []string{"ERROR: ", err.Error()}
	}

	names := []string{}
	for idx := range entries {
		names = append(names, entries[idx].Name())
	}

	return names
}

func ListMedia() []string {	
	return listDir(media_dir)
}

func ListStatic() []string {
	return listDir(static_dir)
}

func ListNuggets() []string {
	return listDir(nugget_dir)
}

func AdminDashHandler(w http.ResponseWriter, r *http.Request) {

	tpl := template.Must(template.ParseFiles(dash_template, base_template))

	//tpl, err := tpl.ExecuteTemplate(dash_str)
	//if err != nil {
	//	err_msg := "failed to load admin dashboard template"
	//	log.Error(err_msg)
	//	http.Error(w, err_msg, http.StatusInternalServerError)
	//}

	session_valid := r.Context().Value("session_valid").(bool)

	data := struct {
		TemplateContext
		Media []string
		Static []string
		Nuggets []string
		TotalRequests int
		StartupTime int64
		Hooks []string
		Sessions *SessionStore
		IPCache IPLocationCache
	}{
		TemplateContext: TemplateContext{
			SessionValid: session_valid,
		},
		Media: ListMedia(),
		Static: ListStatic(),
		Nuggets: ListNuggets(),
		TotalRequests: request_counter.Get(),
		StartupTime: startup_time.Unix(),
		Hooks: []string{
			"hooks/db/save",
			"hooks/nuggets/reload",
			"hooks/templates/reload",
		},
		Sessions: sessions,
		IPCache: location_cache,
	}

	err := tpl.ExecuteTemplate(w, "base", data)
	if err != nil {
		err_msg := "failed to execute admin dashboard template: " + err.Error()
		log.Error(err_msg)
		http.Error(w, err_msg, http.StatusInternalServerError)
	}
	
}


