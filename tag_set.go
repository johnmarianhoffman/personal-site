package main

type TagSet map[string]struct{}

func NewTagSet(tags []string) TagSet {
	t := TagSet(make(map[string]struct{}))

	for idx := range tags {
		t[tags[idx]] = struct{}{}
	}

	return t
}

func (t TagSet) Contains(key string) bool {
	_, ok := t[key]
	return ok
}



