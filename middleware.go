package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

type ResponseCodeWriter struct {
	http.ResponseWriter
	Code int
}

func (w ResponseCodeWriter) WriteHeader(status int) {
	w.ResponseWriter.WriteHeader(status)
	w.Code = status
}

func GeolocationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){

		// Get actual IP addresses 
		remote_addr := r.Header.Get("x-forwarded-for")
		if remote_addr == "" {
			log.Debug("X-FORWARDED-FOR header entry not found")
			remote_addr = r.RemoteAddr
		} else {
			log.Debug("X-FORWARDED-FOR header entry FOUND: ", remote_addr)
		}

		loc := location_cache.Lookup(remote_addr)

		// Insert fields into request context
		ctx := r.Context()
		ctx = context.WithValue(ctx, "remote_addr", remote_addr)
		ctx = context.WithValue(ctx, "ip_location", loc)
		
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		
		request_counter.Increment()

		start := time.Now()

		rw := ResponseCodeWriter{
			ResponseWriter: w,
			Code: http.StatusOK,
		}

		next.ServeHTTP(rw, r)

		// Perform cached GeoIPLookup

		elapsed := time.Since(start)
		log.WithFields(log.Fields{
			"status":   rw.Code,
			"url":      r.URL,
			"method":   r.Method,
			"protocol": r.Proto,
			"remote":   r.Context().Value("remote_addr").(string),
			"location": r.Context().Value("ip_location").(string),
			"elapsed":  elapsed,
			"user-agent":  r.UserAgent(),
		}).Infof(r.URL.Path)

	})
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		session_valid := true

		session_cookie, err := r.Cookie("session_token")
		if err != nil {
			//http.Redirect(w, r, "/", http.StatusFound)
			session_valid = false
		}

		// Short circuit the pointer call if no cookie presented (will panic)
		if session_cookie == nil || !sessions.IsValid(session_cookie.Value) {
			//http.Redirect(w, r, "/", http.StatusFound)
			session_valid = false
		}

		log.Debug("auth middleware invoked")
		
		ctx := r.Context()
		ctx = context.WithValue(ctx, "session_valid", session_valid)
		
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func RedirectUnauthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		ctx := r.Context()
		
		is_authed := ctx.Value("session_valid").(bool)
		if !is_authed {
			log.Warn("unauthed request attempt")
			http.Redirect(w, r, "/", http.StatusFound)
		}
		
		next.ServeHTTP(w, r)
	})
}

