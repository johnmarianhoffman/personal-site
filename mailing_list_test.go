package main

import (
	"testing"
)

func TestMailingList(t *testing.T) {

	m := NewMailingList("test", "you're registering for <listname>", "")
	
	l := ListEntry{
		Name: "John Hoffman",
		Affiliation: "I belong to no one",
		Email: "john@jmh.lol",	
	}
	
	m.Insert(l)
	
	t.Log(m)
	
}
