package main

import (
	"sync"
	"gopkg.in/yaml.v3"
	"path/filepath"
	"os"
	"fmt"
	log "github.com/sirupsen/logrus"
)

type ListEntry struct {
	Name string `yaml:,omitempty`
	Affiliation string `yaml:,omitempty`
	Email string `json:"email"`
}

type MailingList struct {
	Name string
	Note string 
	Image string
	Redirect string
	mu sync.Mutex
	Entries []ListEntry
}

func NewMailingList(name string, note string, img_src string, redirect string) *MailingList {

	m :=  &MailingList{
		Name: name,
		Note: note,
		Redirect: redirect,
		Image: img_src,
		mu: sync.Mutex{},
		Entries: make([]ListEntry, 0),
	}
	
	return m
}

func (m *MailingList) GetFileName() string {
	filename := fmt.Sprintf("mailing-list-%s.yaml", m.Name)
	return filepath.Join(private_dir, filename)
}

// TODO: encrypted reader/writers
func (m *MailingList) Save() error {

	log.Info("saving updated mailing list: ", m.Name)
	
	m.mu.Lock()
	defer m.mu.Unlock()

	f, err := os.Create(m.GetFileName())
	if err != nil {
		return err
	}

	err = yaml.NewEncoder(f).Encode(m)
	if err != nil {
		return err
	}

	return nil
}

func (m *MailingList) Load() error {

	log.Info("loading mailing list: ", m.Name)
	
	m.mu.Lock()
	defer m.mu.Unlock()
	
	f, err := os.Open(m.GetFileName())
	if err != nil {
		return err
	}

	err = yaml.NewDecoder(f).Decode(m)
	if err != nil {
		return err
	}

	return nil
}

func (m *MailingList) Insert(new ListEntry) {
	m.mu.Lock()
	defer m.mu.Unlock()

	m.Entries = append(m.Entries, new)
}

//func (m *MailingList) ServeHTTP(w http.ResponseWriter, r *http.Request) {
//	
//	err := templates["index"].ExecuteTemplate(w, "base", data)
//	if err != nil {
//		log.Error("failed to execute template: ", err)
//	}
//
//}
